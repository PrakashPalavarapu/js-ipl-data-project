const { matchesData, deliveriesData } = require("../../csvJson");
const fs = require("fs");
let yearAndMateches = {};

const data = matchesData;
try {
  if (typeof data !== "object") {
    throw new Error("Data is not in JSON format");
  } else {
    for (let match of data) {
      if (yearAndMateches[match["season"]]) {
        yearAndMateches[match["season"]] += 1;
      } else {
        yearAndMateches[match["season"]] = 1;
      }
    }
    fs.writeFileSync(
      "../public/output/matchesPerYear.json",
      JSON.stringify(yearAndMateches),
      "utf-8",
      (error) => {
        if (error) throw error;
      }
    );
  }
} catch (error) {
  console.error(error.name, error.message);
}
